#-------------------------------------------------
#
# Project created by QtCreator 2012-10-29T09:22:19
#
#-------------------------------------------------

QT       += core gui

TARGET = Assignment
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

win32 {
LIBS += -Lc:/OpenCV/bin -Lc:/OpenCV/lib -lopencv_core242 -lopencv_highgui242 \
-lopencv_imgproc242 -lopencv_features2d242 -lopencv_calib3d242
INCLUDEPATH += c:/OpenCV/include
}
