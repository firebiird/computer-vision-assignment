#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "QPixmap.h"
#include <QFileDialog>

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

}


MainWindow::~MainWindow()
{
       delete ui;
}

int MainWindow::Open()
{

    QString file = QFileDialog::getOpenFileName(this, tr("open"),"",tr("Image Files (*.png *.jpg *.bmp)"));


    image = imread(file.toStdString()); // reads an image file
       if( !image.data ) // check if the image has been loaded properly
       {
           std::cout << "did not load" << std::endl; // prints did not load if the image cold not be found and the aborts the program.
            return -1;
       }
       for(int i = 0;i < 256;i++)
       {
           histArrayL[i] = 0;
           histArraya[i] = 0;
           histArrayb[i] = 0;
       }


       histogram.create(256,256,CV_8UC3);

       for(int x = 0; x < histogram.rows;x++)
       {
           for(int y =0 ; y < histogram.cols;y++)
           {
               histogram.at<Vec3b>(x,y) = (0,0,0);
           }
       }
       brightness = 1;
       contrast = 1;
       colorfulness = 10;

       image.copyTo(tmp_image);


       cvtColor(tmp_image,lab_image,CV_BGR2Lab);
       cvtColor(image,color_image,CV_BGR2Lab);
       cvtColor(tmp_image,unsharp_image,CV_BGR2Lab);


       if(ui->histogram->checkState() == 2)
       {
           display(2);
       }
       else
       {
           display(0);
       }
       return 0;
}


void MainWindow::Brightness(int value)
{
    if(imgLoaded()){
 brightness = value;
 drawImg(); //calls the draw method

 if(ui->histogram->checkState() == 2)
 {
     display(2);
 }
 else
 {
     display(0);
 }
}
    else
    {
        //do nothing.
    }
}

void MainWindow::Contrast(int value)
{
    if(imgLoaded()){
    contrast = value;
     drawImg(); //calls the draw method

     if(ui->histogram->checkState() == 2)
     {
         display(2);
     }
     else
     {
         display(0);
     }
}
    else
    {
        //do nothing
    }
}

void MainWindow::Colorfulness(int value)
{
    if(imgLoaded()){
    colorfulness = value;
    cvtColor(image,color_image,CV_BGR2Lab);

    drawImg();//calls the draw method

    if(ui->histogram->checkState() == 2)
    {
        display(2);
    }
    else
    {
        display(0);
    }
}
    else
    {
        //do nothing
    }
}

void MainWindow::Sharpness(int value)
{
    if(imgLoaded())
    {
        sharpness = value;
         // 2) apply gausian blur to unsharp_image and set value of sigmax,y to be sharpness
        if(sharpness != 0)
        {
            GaussianBlur(unsharp_image,unsharp_image,Size(0,0),sharpness,sharpness);
        }
        else
        {
             cvtColor(image,unsharp_image,CV_BGR2Lab);
            //dont apply the gaussian blur.
        }
        drawImg();

        if(ui->histogram->checkState() == 2)
        {

            display(2);
        }
        else
        {

            display(0);
        }
    }
    else
    {
        //do nothing
    }


}


void MainWindow::saveImg()
{

    if(imgLoaded())
    {
    cvtColor(tmp_image,tmp_image,CV_RGB2BGR);
    QString saveLocation = QFileDialog::getSaveFileName(
            this,
            tr("Save image"),
            QDir::currentPath(),
            tr("Image (*.jpg)") );
    imwrite(saveLocation.toStdString(),tmp_image);
    }
    else
    {
        //do nothing.
    }
    }



void MainWindow::drawImg()
{
    for(int x = 0; x < lab_image.rows;x++)
    {
        for(int y = 0; y < lab_image.cols;y++)
        {
            lab_image.at<Vec3b>(x,y)[0] = saturate_cast<uchar>((((image.at<Vec3b>(x,y)[0]) * contrast)+ brightness) - unsharp_image.at<Vec3b>(x,y)[0] + color_image.at<Vec3b>(x,y)[0]);
            lab_image.at<Vec3b>(x,y)[1] =  saturate_cast<uchar>(((color_image.at<Vec3b>(x,y)[1]-128) * (colorfulness/10))+128);
            lab_image.at<Vec3b>(x,y)[2] = saturate_cast<uchar>(((color_image.at<Vec3b>(x,y)[2]-128) * colorfulness/10)+128);
        }
    }
}


void MainWindow::display(int value)
{
    if(value == 2)//if checkbox is checked show histogram otherwise show picture.
    {
        histogramSelected();

        flip(histogram,histogram,0);


        QImage img = QImage( (const unsigned char*)(histogram.data), //change the mat image to a QImage so that the label can make a pixmap out of it.
        histogram.cols,histogram.rows, histogram.step1(),
        QImage::Format_RGB888 );
        ui->ImView->setPixmap(QPixmap::fromImage(img));
        ui->ImView->setScaledContents(true);
        ui->ImView->resize(ui->ImView->size());




    }
    else
    {

        cvtColor(lab_image,tmp_image,CV_Lab2RGB);
        QImage img = QImage( (const unsigned char*)(tmp_image.data), //change the mat image to a QImage so that the label can make a pixmap out of it.
        tmp_image.cols, tmp_image.rows, tmp_image.step1(),
        QImage::Format_RGB888 );
        ui->ImView->setPixmap(QPixmap::fromImage(img));
        ui->ImView->setScaledContents(true);
        ui->ImView->resize(ui->ImView->size());
    }


}


// this calculates the histogram
void MainWindow::histogramSelected()
{


        //do histogram
        if(imgLoaded())
        {
            display(0);// to refresh the display

            //clears the image back to black.
            for(int x = 0; x < histogram.rows;x++)
            {
                for(int y =0 ; y < histogram.cols;y++)
                {
                    histogram.at<Vec3b>(x,y) = (0,0,0);
                }
            }
            // clears the array.
            for(int i = 0;i < 256;i++)
            {
                histArrayL[i] = 0;
                histArraya[i] = 0;
                histArrayb[i] = 0;
            }


            //loop through each pixel in tmp_image


            for(int c = 0;c < 3;c++)
            {
                for(int x = 0;x < tmp_image.rows;x++)
                {
                    for(int y = 0;y < tmp_image.cols;y++)
                    {
                        if(c == 0)
                        {
                            histArrayL[tmp_image.at<Vec3b>(x,y)[c]] = histArrayL[tmp_image.at<Vec3b>(x,y)[c]] + 1;
                        }//increment the value of each array slot corresponding to the value of the image.
                        else if (c == 1)
                        {
                            histArraya[tmp_image.at<Vec3b>(x,y)[c]] = histArraya[tmp_image.at<Vec3b>(x,y)[c]] + 1;
                        }
                        else
                        {
                            histArrayb[tmp_image.at<Vec3b>(x,y)[c]] = histArrayb[tmp_image.at<Vec3b>(x,y)[c]] + 1;
                        }
                    }
                }
            }
            Vec3b white(255,255,255);

            Vec3b blue(255,0,0);

            Vec3b red(0,0,255);

            for(int i = 0;i < 256;i++)
            {
                    //white line
                   int totalImgSize = (image.cols*image.rows);
                   float f1 = (float)histArrayL[i]/totalImgSize;
                   histogram.at<Vec3b>(f1*256,i) = white;

                   //red
                   float f2 = (float)histArraya[i]/totalImgSize;
                   histogram.at<Vec3b>(f2*256,i) = blue;

                   //blue
                    float f3 = (float)histArrayb[i]/totalImgSize;
                    histogram.at<Vec3b>(f3*256,i) = red;
            }


        }
        else
        {
            //do nothing
        }

}

// checks is the image has been loaded
boolean MainWindow::imgLoaded()
{
    if(!image.data)
    {
        return false;
    }
    else
    {
        return true;
    }
}
